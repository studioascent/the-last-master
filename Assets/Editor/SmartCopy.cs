﻿using UnityEngine;
using UnityEditor;
public class SmartCopy : EditorWindow
{
   GameObject objectToCopy;
   GameObject pastLevelObject;
   GameObject presentLevelObject;
   Vector3 objectV3;
   Vector3 objectScale;

   [MenuItem("Tools/Duplicate Level Objects")]
        public static void ShowWindow()
        {
            GetWindow(typeof(SmartCopy), false, "Dupe Objects", true);
        }

    private void OnGUI()
    {
        GUILayout.Label("Duplicate Parameters", EditorStyles.boldLabel);

        objectToCopy = EditorGUILayout.ObjectField("GameObject to Copy", objectToCopy, typeof(GameObject), true) as GameObject;
        GUILayout.Label("Make sure this is a direct child of your level parent object!");
        pastLevelObject = EditorGUILayout.ObjectField("Past Level Parent", pastLevelObject, typeof(GameObject), true) as GameObject;
        GUILayout.Label("Past Level parent object used by your FSMs.");
        presentLevelObject = EditorGUILayout.ObjectField("Present Level Parent", presentLevelObject, typeof(GameObject), true) as GameObject;
        GUILayout.Label("Present Level parent object used by your FSMs.");

        if (GUILayout.Button("Copy Object to Past"))
        {
            CopyObjectPast();
        }

        if (GUILayout.Button("Copy Object to Present"))
        {
            CopyObjectPresent();
        }

    }

    private void CopyObjectPast()
    {
        if(objectToCopy == null)
        {
            Debug.LogError("Error: Please assign an object to copy.");
            return;
        }
        if(pastLevelObject == null)
        {
            Debug.LogError("Error: Please assign a past level object.");
            return;
        }

        Vector3 objectV3 = objectToCopy.transform.localPosition;
        Quaternion objectRot = objectToCopy.transform.rotation;
        Transform pastLevelTransform = pastLevelObject.transform;
        string objectName = objectToCopy.name;

        GameObject newObject = Instantiate(objectToCopy, objectV3, objectRot);
        newObject.transform.SetParent(pastLevelTransform, false);
        newObject.name = objectName + " Copy";

            Debug.Log("Success: Copied " + objectToCopy.name + " to parent " + pastLevelTransform.name);
            return;
        
    }

    private void CopyObjectPresent()
    {
        if(objectToCopy == null)
        {
            Debug.LogError("Error: Please assign an object to copy.");
            return;
        }
        if(presentLevelObject == null)
        {
            Debug.LogError("Error: Please assign a present level object.");
            return;
        }

        Vector3 objectV3 = objectToCopy.transform.localPosition;
        Quaternion objectRot = objectToCopy.transform.rotation;
        Transform presentLevelTransform = presentLevelObject.transform;
        string objectName = objectToCopy.name;

        GameObject newObject = Instantiate(objectToCopy, objectV3, objectRot);
        newObject.transform.SetParent(presentLevelTransform, false);
        newObject.name = objectName + " Copy";

            Debug.Log("Success: Copied " + objectToCopy.name + " to parent " + presentLevelTransform.name);
            return;
        
    }


}
